﻿using Nest;
using RestShop.Contracts.DTOs;
using Serilog;
using System;

namespace RestShop.ElasticSearch
{
    
    public  class ElasticSearchClient
    {
        private readonly ElasticClient client;
        private readonly ConnectionSettings settings;
        public ElasticSearchClient()
        {
            var elasticUri = new Uri("http://localhost:9200");
            this.settings = new ConnectionSettings(elasticUri)
            .DefaultIndex("order_messages");
            this.client = new ElasticClient(settings);

        }

        public void SendToElastic(string message)
        {
            var messageToSend = new ElasticMessageDto() { Time = DateTime.Now, Message = message };
            var clienResponse = this.client.IndexDocument(messageToSend);
            Log.Logger.Information("Received Message: " + message);
            Log.Logger.Information("Elastic Response: " + clienResponse.ToString());
           
        }
    }
}
