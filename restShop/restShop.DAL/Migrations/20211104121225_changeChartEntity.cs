﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RestShop.DAL.Migrations
{
    public partial class changeChartEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Count", "Description", "Maker", "Name", "Price" },
                values: new object[,]
                {
                    { 1, 0, "none", "None", "testProduct1", 11.2f },
                    { 2, 3, "none", "None", "testProduct22", 1.2f }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Adress", "Email", "IsAdmine", "IsVertified", "Password", "PhoneNumber", "Username" },
                values: new object[,]
                {
                    { 1, "test st, 22", "test@.mail.com", null, null, "123123", "123123", "test99" },
                    { 2, "test st, 22", "test1@.mail.com", null, null, "123123", "123123", "test199" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
