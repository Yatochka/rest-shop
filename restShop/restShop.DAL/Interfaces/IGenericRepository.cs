﻿namespace RestShop.DAL.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    public interface IGenericRepository<TDto>
        where TDto : class
    {
        public IEnumerable<TDto> FindAll(
         Expression<Func<TDto, bool>> filter = null,
         Func<IQueryable<TDto>, IOrderedQueryable<TDto>> orderBy = null,
         List<string> includes = null);

        public TDto GetById(object id);

        public TDto Create(TDto entity);

        public TDto Update(TDto entity);

        public bool Remove(TDto entity);

        public bool Remove(object id);

        abstract bool IsExist(object id, Expression<Func<TDto, bool>> filter = null);
    }
}
