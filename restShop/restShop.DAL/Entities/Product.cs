﻿namespace RestShop.DAL.Entities
{
    using System.ComponentModel.DataAnnotations;

    public class Product
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50, MinimumLength =4)]
        public string Name { get; set; }

        [Required]
        public float Price { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; } = "No description!";

        public string Maker { get; set; } = "None";

        [Required]
        public int Count { get; set; } = 0;
    }
}
