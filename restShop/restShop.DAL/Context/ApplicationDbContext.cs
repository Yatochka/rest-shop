﻿namespace RestShop.DAL.Context
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using RestShop.DAL.Entities;

    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            // this.Database.EnsureDeleted();
            this.Database.EnsureCreated();
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(new User[]
            {
                new User
                    {
                        Id = 1,
                        Email = "test@.mail.com",
                        Username = "test99",
                        Password = "123123",
                        Adress = "test st, 22",
                        PhoneNumber = "123123",
                    },
                new User
                    {
                        Id = 2,
                        Email = "test1@.mail.com",
                        Username = "test199",
                        Password = "123123",
                        Adress = "test st, 22",
                        PhoneNumber = "123123",
                    },
            });

            modelBuilder.Entity<Product>().HasData(new Product[]
            {
                new Product
                {
                    Id = 1,
                    Name = "testProduct1",
                    Price = 11.2f,
                    Description = "none",
                    Count = 0,
                },

                new Product
                {
                    Id = 2,
                    Name = "testProduct22",
                    Price = 1.2f,
                    Description = "none",
                    Count = 3,
                },
            });
        }
    }
}
