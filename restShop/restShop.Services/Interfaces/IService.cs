﻿namespace RestShop.Services.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    public interface IService<TEntity, TDto>
        where TEntity : class
        where TDto : class
    {
        public IEnumerable<TDto> GetAll();

        public TDto GetById(object id, Expression<Func<TDto, bool>> filter = null);

        public TDto CreateEntity(TDto obj);

        public TDto UpdateEntity(object id, TDto obj, Expression<Func<TDto, bool>> filter = null);

        public bool RemoveEntity(object id);
    }
}
