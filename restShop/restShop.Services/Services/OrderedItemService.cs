﻿namespace RestShop.Services.Services
{
    using System;
    using System.Linq.Expressions;
    using Newtonsoft.Json;
    using RestShop.Contracts.DTOs;
    using RestShop.DAL.Entities;
    using RestShop.DAL.Interfaces;

    public class OrderedItemService : BaseService<OrderedItem, OrderedItemDto>
    {
        public OrderedItemService(IGenericRepository<OrderedItemDto> rep)
            : base(rep)
        {
        }

        public override OrderedItemDto GetById(object id, Expression<Func<OrderedItemDto, bool>> filter = null)
        {
            return base.GetById(id, q => q.Id == id.ToString());
        }

        public override OrderedItemDto CreateEntity(OrderedItemDto obj)
        {
            var productString = HttpClientService.GetData($"api/Products/{obj.ProductId}");
            var product = JsonConvert.DeserializeObject<ProductDto>(productString);
            if ((product.Count - obj.Amount) < 0)
            {
                throw new ArgumentOutOfRangeException("Not enough products in the store");
            }

            obj.Product = product;
            base.CreateEntity(obj);

            product.Count -= obj.Amount;
            if (product.Count < 0)
            {
                product.Count = 0;
            }

            var updProduct = JsonConvert.SerializeObject(product);
            HttpClientService.SendData($"api/Products/{obj.ProductId}", updProduct);

            return this.GetById(obj.Id);
        }

        public override OrderedItemDto UpdateEntity(object id, OrderedItemDto obj, Expression<Func<OrderedItemDto, bool>> filter = null)
        {
            return base.UpdateEntity(id, obj, q => q.Id == id.ToString());
        }
    }
}