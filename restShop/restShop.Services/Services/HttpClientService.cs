﻿namespace RestShop.Services.Services
{
    using System;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using Polly;
    using Polly.Retry;

    public static class HttpClientService
    {
        private const string BASEURI = "https://localhost:5001/";
        private static readonly HttpClient Client;
        private static readonly RetryPolicy RetryPolicy;

        static HttpClientService()
        {
             Client = new HttpClient
             {
                 BaseAddress = new Uri(BASEURI),
             };
             Client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
             RetryPolicy = Polly.Policy.Handle<HttpRequestException>().WaitAndRetry(new[]
               {
                   TimeSpan.FromSeconds(2),
                   TimeSpan.FromSeconds(4),
                   TimeSpan.FromSeconds(8),
               });
        }

        public static string GetData(string uri)
        {
            HttpResponseMessage response = Client.GetAsync(uri).Result;

            if (response.IsSuccessStatusCode)
            {
                return RetryHelper(() =>
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    return content;
                });
            }

            return null;
        }

        public static void SendData(string uri, string serializedObj)
        {
            HttpContent content = new StringContent(serializedObj, Encoding.UTF8, "application/json");
            RetryHelper(() => { Client.PutAsync(uri, content); });
        }

        private static string RetryHelper(Func<string> action)
        {
            return RetryPolicy.Execute(action);
        }

        private static void RetryHelper(Action action)
        {
             RetryPolicy.Execute(action);
        }
    }
}
