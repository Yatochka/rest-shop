﻿namespace RestShop.Services.Services
{
    using System;
    using System.Linq.Expressions;
    using RestShop.Contracts.DTOs;
    using RestShop.DAL.Entities;
    using RestShop.DAL.Interfaces;
    using RestShop.DAL.SQL.Interfaces;

    public class ProductService : BaseService<Product, ProductDto>
    {
        public ProductService(IGenericRepository<ProductDto> rep)
            : base(rep)
        {
        }

        public override ProductDto GetById(object id, Expression<Func<ProductDto, bool>> filter = null)
        {
            return base.GetById(id, q => q.Id == (int)id);
        }

        public override ProductDto UpdateEntity(object id, ProductDto obj, Expression<Func<ProductDto, bool>> filter = null)
        {
            return base.UpdateEntity(id, obj, q => q.Id == (int)id);
        }
    }
}
