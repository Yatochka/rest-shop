﻿namespace RestShop.Services.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using Newtonsoft.Json;
    using RestShop.Contracts.DTOs;
    using RestShop.DAL.Entities;
    using RestShop.DAL.Interfaces;
    using RestShop.DAL.Mongo.Interfaces;
    using Serilog;

    public class CartService : BaseService<Cart, CartDto>
    {
        private readonly RabbitMQClient rabbitClient;
        public CartService(IGenericRepository<CartDto> rep)
            : base(rep)
        {
            this.rabbitClient = new RabbitMQClient();
        }

        public override CartDto GetById(object id, Expression<Func<CartDto, bool>> filter = null)
        {
            // need to move update call to ordered item remove method
            var cart = base.GetById(id, q => q.Id == id.ToString());
            this.UpdateEntity(id, cart);
            return cart;
        }

        public override CartDto CreateEntity(CartDto obj)
        {
            this.UpdateEntityForUser(ref obj);
            var ent = base.CreateEntity(obj);
            this.CreateRabbitMesssage(ent);
            return ent;
        }

        public override CartDto UpdateEntity(object id, CartDto obj, Expression<Func<CartDto, bool>> filter = null)
        {
            this.UpdateEntityForUser(ref obj);
            return base.UpdateEntity(id, obj, q => q.Id == id.ToString());
        }

        private void UpdateEntityForUser(ref CartDto obj)
        {
            var user = HttpClientService.GetData($"api/Users/{obj.UserId}");
            var deserializedUser = JsonConvert.DeserializeObject<UserViewDto>(user);
            obj.User = deserializedUser;
            List<OrderedItemDto> orders = JsonConvert.DeserializeObject<List<OrderedItemDto>>(HttpClientService.GetData($"api/OrderedItems"));
            var userOrders = orders.FindAll(q => q.UserId == deserializedUser.Id);
            obj.OrderedItem = userOrders;
        }

        private void CreateRabbitMesssage(CartDto enity)
        {
            var message = string.Format("Order {0} was created for user {1} with Id {2}. Order status {3}", enity.Id, enity.User.Username, enity.UserId, enity.Status);
            Log.Logger.Information(message);
            this.rabbitClient.SendRabbitMessage(message);
        }
    }
}
