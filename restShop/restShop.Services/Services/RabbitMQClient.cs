﻿namespace RestShop.Services.Services
{
    using System;
    using System.Text;
    using RabbitMQ.Client;
    using RabbitMQ.Client.Events;
    using RestShop.Contracts.Delegates;
    using Serilog;

    public class RabbitMQClient
    {
        private static ConnectionFactory connectionFactory;
        private static IConnection connection;
        private static IModel channel;

        public RabbitMQClient()
        {
            connectionFactory = new ConnectionFactory() { HostName = "localhost" };
            connection = connectionFactory.CreateConnection();
            channel = connection.CreateModel();
            channel.QueueDeclare(
                queue: "order",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);
        }

        public void SendRabbitMessage(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);
            Log.Logger.Information(message);

            channel.BasicPublish(
                exchange: string.Empty,
                routingKey: "order",
                basicProperties: null,
                body: body);
        }

        public void ReceiveRabbitMessage(MessageSender sender)
        {
            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                sender(message);

                // sender.GetInvocationList().Select(x => (int)x.DynamicInvoke(message));

                Console.WriteLine("[x] received message:" + message);
            };

            channel.BasicConsume(
                queue: "order",
                autoAck: true,
                consumer: consumer);
        }

        public void Dispose()
        {
            channel.Close();
        }
    }
}
