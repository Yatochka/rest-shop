﻿namespace RestShop.Services.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using RestShop.DAL.Interfaces;
    using RestShop.Services.Interfaces;

    public abstract class BaseService<TEntity, TDto> : IService<TEntity, TDto>
        where TEntity : class
        where TDto : class
    {
        private readonly IGenericRepository<TDto> rep;

        public BaseService(IGenericRepository<TDto> rep)
        {
            this.rep = rep;
        }

        public virtual TDto CreateEntity(TDto obj)
        {
            return this.rep.Create(obj);
        }

        public virtual IEnumerable<TDto> GetAll()
        {
            var res = this.rep.FindAll();
            return res;
        }

        public virtual TDto GetById(object id, Expression<Func<TDto, bool>> filter = null)
        {
                if (!this.rep.IsExist(id, filter))
                {
                    throw new Exception("Object is not found");
                }

                var res = this.rep.GetById(id);

                return res;
        }

        public virtual TDto UpdateEntity(object id, TDto obj, Expression<Func<TDto, bool>> filter = null)
        {
            if (!this.rep.IsExist(id, filter))
            {
                throw new Exception("Object is not found");
            }

            return this.rep.Update(obj);
        }

        public bool RemoveEntity(object id)
        {
            try
            {
                return this.rep.Remove(id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
    }
}
