﻿namespace RestShop.Services.Services
{
    using System;
    using System.Linq.Expressions;
    using RestShop.Contracts.DTOs;
    using RestShop.DAL.Entities;
    using RestShop.DAL.Interfaces;
    using RestShop.DAL.SQL.Interfaces;

    public class UserService : BaseService<User, UserDto>
    {
        public UserService(IGenericRepository<UserDto> rep)
            : base(rep)
        {
        }

        public override UserDto GetById(object id, Expression<Func<UserDto, bool>> filter = null)
        {
            return base.GetById(id, q => q.Id == (int)id);
        }

        public override UserDto UpdateEntity(object id, UserDto obj, Expression<Func<UserDto, bool>> filter = null)
        {
            return base.UpdateEntity(id, obj, q => q.Id == (int)id);
        }
    }
}
