﻿namespace RestShop.Contracts.ShopMapper
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using AutoMapper;
    using RestShop.Contracts.Mapping;

    public class ShopMapper<TEntity, TDto>
        where TEntity : class
        where TDto : class
    {
        private readonly IMapper mapper;
        private readonly MapperConfiguration configuration;

        public ShopMapper()
        {
            this.configuration = new MapperConfiguration(cfg => cfg.AddProfile(new Maps()));
            this.mapper = new Mapper(this.configuration);
        }

        public TDto MapEntityToDto(TEntity obj)
        {
            return this.mapper.Map<TDto>(obj);
        }

        public TEntity MapDtoToEntity(TDto obj)
        {
            return this.mapper.Map<TEntity>(obj);
        }

        public IEnumerable<TDto> MapToDtoList(IEnumerable<TEntity> query)
        {
            return this.mapper.Map<IEnumerable<TDto>>(query);
        }

        public Expression<Func<TEntity, bool>> ConvertFilter(Expression<Func<TDto, bool>> obj)
        {
            return this.mapper.Map<Expression<Func<TEntity, bool>>>(obj);
        }
    }
}
