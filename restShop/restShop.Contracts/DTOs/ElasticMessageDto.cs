﻿namespace RestShop.Contracts.DTOs
{
    using System;

    public class ElasticMessageDto
    {
        public string Message { get; set; }

        public DateTime Time { get; set; }
    }
}
