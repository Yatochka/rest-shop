﻿namespace RestShop.Contracts.Mapping
{
    using System;
    using System.Linq.Expressions;
    using AutoMapper;
    using RestShop.Contracts.DTOs;
    using RestShop.DAL.Entities;

    public class Maps : Profile
    {
        public Maps()
        {
            this.CreateMap<UserDto, UserViewDto>().ReverseMap();
            this.CreateMap<UserViewDto, User>().ReverseMap();
            this.CreateMap<User, UserDto>().ReverseMap();
            this.CreateMap<Product, ProductDto>().ReverseMap();
            this.CreateMap<OrderedItem, OrderedItemDto>().ReverseMap();
            this.CreateMap<Cart, CartDto>().ReverseMap();
        }
    }
}
