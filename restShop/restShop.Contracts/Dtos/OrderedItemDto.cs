﻿namespace RestShop.Contracts.DTOs
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using MongoDB.Bson.Serialization.Attributes;

    public class OrderedItemDto
    {
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string Id { get; set; }

        [Required]
        public int UserId { get; set; } = 1;

        [Required]
        public int ProductId { get; set; } = 1;

        public ProductDto Product { get; set; }

        [Required]
        [Range(0, 1000000)]
        public int Amount { get; set; }
    }
}
