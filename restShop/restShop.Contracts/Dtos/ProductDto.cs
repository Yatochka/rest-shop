﻿namespace RestShop.Contracts.DTOs
{
    public class ProductDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public float Price { get; set; }

        public string Description { get; set; }

        public string Maker { get; set; }

        public int Count { get; set; }
    }
}
