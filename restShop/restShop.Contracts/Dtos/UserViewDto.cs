﻿namespace RestShop.Contracts.DTOs
{
    using System;

    [Serializable]
    public class UserViewDto
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string Adress { get; set; }

        public string PhoneNumber { get; set; }
    }
}
