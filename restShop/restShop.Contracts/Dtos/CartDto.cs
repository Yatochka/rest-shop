﻿namespace RestShop.Contracts.DTOs
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using MongoDB.Bson.Serialization.Attributes;

    public class CartDto
    {
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string Id { get; set; }

        [Required]
        public int UserId { get; set; } = 1;

        public UserViewDto User { get; set; }

        public List<OrderedItemDto> OrderedItem { get; set; }

        public string Status { get; set; } = "Init";
    }
}
