﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace restShop.Logger.Loggers
{
    public class ProjectLogger
    {
        private readonly ILogger _logger;

        public ProjectLogger(ILogger logger)
        {
            this._logger = logger;
        }

        public void LogInfo(string message)
        {
            this._logger.LogInformation(message);
        }

        public void LogError(Exception ex, string message)
        {
            this._logger.LogError(ex, message);
        }

        public void LogWarnings(string message)
        {
            this._logger.LogWarning(message);
        }
    }
}
