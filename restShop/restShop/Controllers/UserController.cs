﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace restShop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {/*
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _context;
        public UserController(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            _unitOfWork = new UnitOfWork(_context);
        }
        // GET: api/<UserController>
        [HttpGet]
        public IActionResult Get()
        {

            var result = _unitOfWork.Users.FindAll();

            if (result == null)
                return NotFound("Query is empty!");

            var dto = _mapper.Map<List<UserDto>>(result);

            return Ok(dto);
        }

        // GET api/<UserController>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            if (!_unitOfWork.Users.isExist(id, q=>q.Id==id))
                return NotFound("User is not found!");

            var result = _unitOfWork.Users.GetById(id);
            var dto = _mapper.Map<UserDto>(result);
            return Ok(dto);
        }

        // POST api/<UserController>
        [HttpPost]
        public IActionResult Post(UserDto userdto)
        {
            try
            {
                if (userdto == null)
                    return BadRequest("Object is empty!");

                var model = _mapper.Map<User>(userdto);
                _unitOfWork.Users.Create(model);
                _unitOfWork.Save();
                return Ok();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return BadRequest(e.Message);
            }
        }

        // PUT api/<UserController>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, UserDto userdto)
        {

            try
            {
                if (id != userdto.Id)
                    return BadRequest("Id from URL doesn't match with User id!");

                if (!_unitOfWork.Users.isExist(id, q => q.Id == id))
                    return NotFound("User is not found!");

                var model = _mapper.Map<User>(userdto);
                _unitOfWork.Users.Update(model);
                _unitOfWork.Save();
                return Ok();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return BadRequest(e.Message);
            }
        }

        // DELETE api/<UserController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (!_unitOfWork.Users.isExist(id, q => q.Id == id))
                return NotFound("User is not found!");

            _unitOfWork.Users.Remove(id);
            _unitOfWork.Save();
            return Ok("User successfully deleted!");

        }*/
    }
}
