﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace restShop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {/*
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _context;
        public ProductController(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            _unitOfWork = new UnitOfWork(_context);
        }
        // GET: api/<ProductController>
        [HttpGet]
        public IActionResult Get()
        {
            var result = _unitOfWork.Products.FindAll();

            if (result == null)
                return NotFound("Query is empty!");

            var dto = _mapper.Map<List<ProductDto>>(result);
            return Ok(dto);
        }

        // GET api/<ProductController>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var result = _unitOfWork.Products.GetById(id);

            if (!_unitOfWork.Products.isExist(id, q => q.Id == id))
                return NotFound("Ordered item is not found!");

            var dto = _mapper.Map<ProductDto>(result);
            return Ok(dto);
        }

        // POST api/<ProductController>
        [HttpPost]
        public IActionResult Post(ProductDto productdto)
        {
            try
            {
                if (productdto == null)
                    return BadRequest("Object is empty!");


                var model = _mapper.Map<Product>(productdto);
                _unitOfWork.Products.Create(model);
                _unitOfWork.Save();
                return Ok();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return BadRequest(e.Message);
            }
        }

        // PUT api/<ProductController>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, ProductDto productdto)
        {

            try
            {
                if (id != productdto.Id)
                   return BadRequest("Id from Uri and Object don't match!");

                if (!_unitOfWork.Products.isExist(id, q => q.Id == id))
                    return NotFound("Ordered item is not found!");

                var model = _mapper.Map<Product>(productdto);
                _unitOfWork.Products.Update(model);
                _unitOfWork.Save();
                return Ok();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return BadRequest(e.Message);
            }
        }

        // DELETE api/<ProductController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (!_unitOfWork.Products.isExist(id, q => q.Id == id))
                return NotFound("Ordered item is not found!");

            _unitOfWork.Products.Remove(id);
            _unitOfWork.Save();

            return Ok("Successfully deleted!");
        }*/
    }
}
