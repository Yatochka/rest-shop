﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace restShop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderedItemController : ControllerBase
    {/*
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _context;

        public OrderedItemController(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            _unitOfWork = new UnitOfWork(_context);
        }
        // GET: api/<OrderedItemController>
        [HttpGet]
        public IActionResult Get()
        {
            var result = _unitOfWork.OrderedItems.FindAll();

            if (result == null)
                return NotFound("Query is empty!");

            var dto = _mapper.Map<List<OrderedItemDto>>(result);
            return Ok(dto);
        }

        // GET api/<OrderedItemController>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            if (!_unitOfWork.OrderedItems.isExist(id, q => q.Id == id))
                return NotFound("Ordered item is not found!");

            var dto = _mapper.Map<OrderedItemDto>(_unitOfWork.OrderedItems.GetById(id));
            return Ok(dto);
        }

        // POST api/<OrderedItemController>
        [HttpPost]
        public IActionResult Post(OrderedItemDto itemdto)
        {
            try
            {
                if (itemdto == null)
                    return BadRequest("Object is empty!");

                var model = _mapper.Map<OrderedItem>(itemdto);
                _unitOfWork.OrderedItems.Create(model);
                _unitOfWork.Save();
                 
                return Ok();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return BadRequest(e.Message);
            }
        }

        // PUT api/<OrderedItemController>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, OrderedItemDto itemdto)
        {
            try
            {
                if (itemdto == null)
                    return BadRequest("Id from Uri and Object don't match!");

                if (!_unitOfWork.OrderedItems.isExist(id, q => q.Id == id))
                    return NotFound("Ordered item is not found!");

                var model = _mapper.Map<OrderedItem>(itemdto);
                _unitOfWork.OrderedItems.Update(model);
                _unitOfWork.Save();

                return Ok();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return BadRequest(e.Message);
            }
        }

        // DELETE api/<OrderedItemController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (!_unitOfWork.OrderedItems.isExist(id, q => q.Id == id))
                return NotFound("Ordered item is not found!");

            _unitOfWork.OrderedItems.Remove(id);
            _unitOfWork.Save();

            return Ok("Successfully deleted!");
        }*/
    }
}
