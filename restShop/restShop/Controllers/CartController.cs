﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace restShop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {/*
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _context;

        public CartController( ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _unitOfWork = new UnitOfWork(context);
            _mapper = mapper;
        }

        // GET: api/<CartController>
        [HttpGet]
        public IActionResult Get()
        { 
            var result = _unitOfWork.Carts.FindAll();

            if (result == null)
                return NotFound("Query is empty!");

            var dto = _mapper.Map<List<CartDto>>(result);

            return Ok(dto);
        }
        // GET api/<CartController>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            if (!_unitOfWork.Carts.isExist(id, q => q.Id == id)) 
                return NotFound("Cart is not found!");

            var result = _unitOfWork.Carts.GetById(id);
            var dto = _mapper.Map<CartDto>(result);
            return Ok(dto);
           
        }
        // GET api/<CartController>/user/5
        [HttpGet("user/{id}")]
        public IActionResult GetByUserId(int id)
        {   
            try
            {
                if (!_unitOfWork.Carts.isExist(id, q => q.UserId == id))
                    return NotFound("There is no carts for this user!");

                if (id < 1)
                    throw new IndexOutOfRangeException();

                var res = _unitOfWork.Carts.FindAll(q => q.UserId == id);
                var dto = _mapper.Map <List<CartDto>>(res);
                return Ok(dto);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return BadRequest(e.Message);
            }

        }

        // POST api/<CartController>
        [HttpPost]
        public IActionResult Post(CartDto cartdto)
        {
            try
            {
                if (cartdto == null)
                    return BadRequest("Object is empty!");

                var model = _mapper.Map<Cart>(cartdto);

                _unitOfWork.Carts.Create(model);
                _unitOfWork.Save();
                return Ok();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return BadRequest(e.Message);
            }
        }

        // PUT api/<CartController>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, CartDto cartdto)
        {

            try
            {
                if (id != cartdto.Id)
                    return BadRequest("Id from Uri and Object don't match!");

                if (!_unitOfWork.Carts.isExist(id, q => q.Id == id))
                    return NotFound("Ordered item is not found!");

                var model = _mapper.Map<Cart>(cartdto);
                _unitOfWork.Carts.Update(model);
                _unitOfWork.Save();
                return Ok();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return BadRequest(e.Message);
            }
        }

        // DELETE api/<CartController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (!_unitOfWork.Carts.isExist(id, q => q.Id == id))
                return NotFound("Ordered item is not found!");

            _unitOfWork.Carts.Remove(id);
            _unitOfWork.Save();
            return Ok("Successfully deleted!");
        }*/
    }
}
