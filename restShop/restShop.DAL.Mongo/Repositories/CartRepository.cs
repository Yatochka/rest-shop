﻿namespace RestShop.DAL.Mongo.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    using MongoDB.Bson;
    using MongoDB.Driver;
    using RestShop.Contracts.DTOs;
    using RestShop.DAL.Interfaces;
    using RestShop.DAL.Mongo.Interfaces;

    public class CartRepository : IMongoRepository<CartDto>
    {
        private readonly IMongoCollection<CartDto> collection;
        private readonly IMongoDatabase database;

        public CartRepository(IMongoDbSettings settings)
        {
            this.database = DbConnection.MongoDbConnection.GetDatabase(settings.ConnectionString, settings.DatabaseName);

            this.collection = this.database.GetCollection<CartDto>("CartDto");
        }

        public CartDto Create(CartDto entity)
        {
            entity.Id = ObjectId.GenerateNewId().ToString();
            this.collection.InsertOne(entity);
            return entity;
        }

        public IEnumerable<CartDto> FindAll(Expression<Func<CartDto, bool>> filter = null, Func<IQueryable<CartDto>, IOrderedQueryable<CartDto>> orderBy = null, List<string> includes = null)
        {
            var query = this.collection.Find(q => true).ToList();
            return query;
        }

        public CartDto GetById(object id)
        {
            return this.collection.Find(q => q.Id == id.ToString()).FirstOrDefault();
        }

        public bool Remove(CartDto entity)
        {
            this.collection.DeleteOne(q => q.Id == entity.Id);
            return true;
        }

        public bool Remove(object id)
        {
            var toDelete = this.collection.Find(q => q.Id == id.ToString()).FirstOrDefault();
            return this.Remove(toDelete);
        }

        public CartDto Update(CartDto entity)
        {
            this.collection.ReplaceOne(q => q.Id == entity.Id, entity);
            return entity;
        }

        public bool IsExist(object id, Expression<Func<CartDto, bool>> filter = null)
        {
            return this.collection.Find(filter).Any();
        }
    }
}
