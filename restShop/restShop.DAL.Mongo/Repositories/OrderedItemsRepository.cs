﻿namespace RestShop.DAL.Mongo.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using MongoDB.Bson;
    using MongoDB.Driver;
    using RestShop.Contracts.DTOs;
    using RestShop.DAL.Interfaces;
    using RestShop.DAL.Mongo.Interfaces;

    public class OrderedItemsRepository : IMongoRepository<OrderedItemDto>
    {
        private readonly IMongoCollection<OrderedItemDto> collection;
        private readonly IMongoDatabase database;

        public OrderedItemsRepository(IMongoDbSettings settings)
        {
            this.database = DbConnection.MongoDbConnection.GetDatabase(settings.ConnectionString, settings.DatabaseName);

            this.collection = this.database.GetCollection<OrderedItemDto>("OrderedItemDto");
        }

        public OrderedItemDto Create(OrderedItemDto entity)
        {
            entity.Id = ObjectId.GenerateNewId().ToString();
            this.collection.InsertOne(entity);
            return entity;
        }

        public IEnumerable<OrderedItemDto> FindAll(Expression<Func<OrderedItemDto, bool>> filter = null, Func<IQueryable<OrderedItemDto>, IOrderedQueryable<OrderedItemDto>> orderBy = null, List<string> includes = null)
        {
            return this.collection.Find(q => true).ToList();
        }

        public OrderedItemDto GetById(object id)
        {
            return this.collection.Find(q => q.Id == id.ToString()).FirstOrDefault();
        }

        public bool IsExist(object id, Expression<Func<OrderedItemDto, bool>> filter = null)
        {
            return this.collection.Find(filter).Any();
        }

        public bool Remove(OrderedItemDto entity)
        {
            this.collection.DeleteOne(q => q.Id == entity.Id);
            return true;
        }

        public bool Remove(object id)
        {
            var toDelete = this.collection.Find(q => q.Id == id.ToString()).FirstOrDefault();
            return this.Remove(toDelete);
        }

        public OrderedItemDto Update(OrderedItemDto entity)
        {
            this.collection.ReplaceOne(q => q.Id == entity.Id, entity);
            return entity;
        }
    }
}
