﻿namespace RestShop.DAL.Mongo.Settings
{
    using RestShop.DAL.Mongo.Interfaces;

    public class MongoDbSettings : IMongoDbSettings
    {
        public string DatabaseName { get; set; }

        public string ConnectionString { get; set; }
    }
}
