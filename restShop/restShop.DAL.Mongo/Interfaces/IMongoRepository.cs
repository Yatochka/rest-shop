﻿namespace RestShop.DAL.Mongo.Interfaces
{
    using RestShop.DAL.Interfaces;

    public interface IMongoRepository<TDto> : IGenericRepository<TDto>
        where TDto : class
    {
    }
}
