﻿namespace DbConnection
{
    using MongoDB.Driver;

    public static class MongoDbConnection
    {
        private static IMongoClient client;
        private static IMongoDatabase database;

        public static IMongoDatabase GetDatabase(string connectionString, string databaseName)
        {
            if (database != null)
            {
                return database;
            }

            client = new MongoClient(connectionString);
            database = client.GetDatabase(databaseName);
            return database;
        }
    }
}
