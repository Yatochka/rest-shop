// <copyright file="Startup.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace RestShop.WebApi
{
    using DbConnection;
    using EmailMessageBus;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Microsoft.Net.Http.Headers;
    using RestShop.Contracts.DTOs;
    using RestShop.Contracts.Mapping;
    using RestShop.DAL.Context;
    using RestShop.DAL.Entities;
    using RestShop.DAL.Interfaces;
    using RestShop.DAL.Mongo.Interfaces;
    using RestShop.DAL.Mongo.Repositories;
    using RestShop.DAL.Mongo.Settings;
    using RestShop.DAL.SQL.Interfaces;
    using RestShop.DAL.SQL.Repository;
    using RestShop.Services.Services;
    using RestShop.WebApi.Controllers;
    using Serilog;
    using Unity;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(this.Configuration.GetConnectionString("DefaultConnection")));

            services.Configure<MongoDbSettings>(this.Configuration.GetSection(nameof(MongoDbSettings)));
            services.Configure<EmailSenderConfigs>(this.Configuration.GetSection(nameof(EmailSenderConfigs)));
            services.AddSingleton<IMongoDbSettings>(sp =>
                sp.GetRequiredService<IOptions<MongoDbSettings>>().Value);
            services.AddHostedService<EmailMessageBus.MessageBus>();
            services.AddAutoMapper(typeof(Maps));
            services.AddSwaggerGen();
            services.AddControllers(options =>
            {
                options.FormatterMappings.SetMediaTypeMappingForFormat("xml", MediaTypeHeaderValue.Parse("application/xml"));
            })
                .AddControllersAsServices()
                .AddXmlSerializerFormatters();
        }

        public void ConfigureContainer(IUnityContainer container)
        {
            container.RegisterType<IGenericRepository<UserDto>, ISQLRepository<UserDto, User>>();
            container.RegisterType<IGenericRepository<ProductDto>, ISQLRepository<ProductDto, Product>>();
            container.RegisterType<IGenericRepository<OrderedItemDto>, IMongoRepository<OrderedItemDto>>();
            container.RegisterType<IGenericRepository<CartDto>, IMongoRepository<CartDto>>();
            container.RegisterType<ISQLRepository<ProductDto, Product>, SqlGenericRepository<ProductDto, Product>>();
            container.RegisterType<ISQLRepository<UserDto, User>, SqlGenericRepository<UserDto, User>>();
            container.RegisterType<IMongoRepository<OrderedItemDto>, OrderedItemsRepository>();
            container.RegisterType<IMongoRepository<CartDto>, CartRepository>();
            container.RegisterType<CartsController>();
            container.RegisterType<UsersController>();
            container.RegisterType<OrderedItemsController>();
            container.RegisterType<ProductsController>();
            container.RegisterType<CartService>();
            container.RegisterType<OrderedItemService>();
            container.RegisterType<ProductService>();
            container.RegisterType<UserService>();
            container.RegisterType<ILoggerFactory, LoggerFactory>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSerilogRequestLogging();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Api v1"));
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
