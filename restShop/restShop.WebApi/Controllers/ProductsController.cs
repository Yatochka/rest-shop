﻿// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RestShop.WebApi.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json;
    using RestShop.Contracts.DTOs;
    using RestShop.Services.Services;
    using Serilog;
    using Unity;

    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ProductService service;

        public ProductsController(IUnityContainer container)
        {
            this.service = container.Resolve<ProductService>();
        }

        // GET: api/<UserController>
        [HttpGet]
        public ActionResult<IEnumerable<ProductDto>> Get()
        {
            try
            {
                var res = this.service.GetAll();

                if (res.Count() > 0)
                {
                    return this.Ok(res);
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "An error occured!");
            }

            return this.NotFound("404, Not Found any object!");
        }

        // GET api/<UserController>/5
        [HttpGet("{id}")]
        public ActionResult<ProductDto> Get(int id)
        {
            try
            {
                var res = this.service.GetById(id);

                if (res != null)
                {
                    Log.Logger.Information("User was found: {0}", JsonConvert.SerializeObject(res, Formatting.None));

                    return this.Ok(res);
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "An error occured!");
            }

            return this.NotFound();
        }

        // POST api/<UserController>
        [HttpPost]
        public ActionResult Post(ProductDto entity)
        {
            try
            {
                this.service.CreateEntity(entity);
                Log.Logger.Information(JsonConvert.SerializeObject(entity, Formatting.None));

                return this.Ok("The object was created!");
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "An error occured!");
            }

            return this.BadRequest();
        }

        // PUT api/<UserController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, ProductDto entity)
        {
            try
            {
                this.service.UpdateEntity(id, entity);

                Log.Logger.Information(JsonConvert.SerializeObject(entity, Formatting.None));

                return this.Ok("The object was updated!");
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "An error occured!");
            }

            return this.BadRequest();
        }

        // DELETE api/<UserController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                var toDelete = this.service.GetById(id);

                if (toDelete != null)
                {
                    this.service.RemoveEntity(id);

                    Log.Logger.Information("Object with id {0} was deleted! {1}", id, JsonConvert.SerializeObject(toDelete, Formatting.None));

                    return this.Ok(toDelete + " is deleted!");
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "An error occured!");
            }

            return this.NotFound();
        }
    }
}
