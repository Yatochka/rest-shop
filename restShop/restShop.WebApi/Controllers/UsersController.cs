﻿// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RestShop.WebApi.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json;
    using RestShop.Contracts.DTOs;
    using RestShop.Services.Services;
    using Serilog;
    using Unity;

    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService service;

        public UsersController(IUnityContainer container)
        {
            this.service = container.Resolve<UserService>();
        }

        // GET: api/<UserController>
        [HttpGet]
        public ActionResult<IEnumerable<UserViewDto>> Get()
        {
            try
            {
                var res = this.service.GetAll();
                List<UserViewDto> dto = new List<UserViewDto>();
                foreach (var obj in res)
                {
                    dto.Add(new UserViewDto { Id = obj.Id, Username = obj.Username, Email = obj.Email, PhoneNumber = obj.PhoneNumber });
                }

                if (dto.Count() > 0)
                {
                    return this.Ok(dto);
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "An error occured!");
            }

            return this.NotFound("404, Not Found any object!");
        }

        // GET api/<UserController>/5
        [HttpGet("{id}")]
        public ActionResult<UserViewDto> Get(int id)
        {
            try
            {
                var res = this.service.GetById(id);

                if (res != null)
                {
                    UserViewDto dto = new UserViewDto { Id = res.Id, Username = res.Username, Email = res.Email, PhoneNumber = res.PhoneNumber };
                    Log.Logger.Information("User was found: {0}", JsonConvert.SerializeObject(dto, Formatting.None));
                    return this.Ok(dto);
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "An error occured!");
            }

            return this.NotFound();
        }

        // POST api/<UserController>
        [HttpPost]
        public ActionResult Post(UserDto entity)
        {
            try
            {
                this.service.CreateEntity(entity);
                Log.Logger.Information(JsonConvert.SerializeObject(entity, Formatting.None));
                return this.Ok("The user was created!");
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "An error occured!");
            }

            return this.BadRequest();
        }

        // PUT api/<UserController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, UserDto entity)
        {
            try
            {
                this.service.UpdateEntity(id, entity);

                Log.Logger.Information(JsonConvert.SerializeObject(entity, Formatting.None));

                return this.Ok("The object was updated!");
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "An error occured!");
            }

            return this.BadRequest();
        }

        // DELETE api/<UserController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                var toDelete = this.service.GetById(id);

                if (toDelete != null)
                {
                    this.service.RemoveEntity(id);

                    Log.Logger.Information("Object with id {0} was deleted! {1}", id, JsonConvert.SerializeObject(toDelete, Formatting.None));

                    return this.Ok(toDelete + " is deleted!");
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "An error occured!");
            }

            return this.NotFound();
        }
    }
}
