﻿namespace RestShop.Tests.ServiceTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using RestShop.Contracts.DTOs;
    using RestShop.DAL.Interfaces;
    using RestShop.Services.Services;

    [TestClass]
    public class CartServiceTests
    {
        private CartService service;

        [TestMethod]
        public void GetCartTest()
        {
            var mockRep = new Mock<IGenericRepository<CartDto>>();
            var carts = this.GetCarts();
            mockRep.Setup(repo => repo.FindAll(null, null, null)).Returns(carts).Verifiable();

            this.service = new CartService(mockRep.Object);

            List<CartDto> serviceResult = (List<CartDto>)this.service.GetAll();

            Assert.AreEqual(serviceResult.Count, 2);
        }

        //Check later. Need mock for http client
        /*

        [TestMethod]
        public void GetCartByIdTest()
        {
            var mockRep = new Mock<IGenericRepository<CartDto>>();
            var carts = this.GetCarts();

            object id = "6184fff9414f7e085a8b8733";

            mockRep.Setup(repo => repo.IsExist(id, q => q.Id == id.ToString())).Returns(true).Verifiable();
            mockRep.Setup(repo => repo.GetById(id)).Returns(carts[0]).Verifiable();

            this.service = new CartService(mockRep.Object);

            CartDto serviceResult = this.service.GetById("6184fff9414f7e085a8b8733");

            Assert.AreEqual(serviceResult.Id, "6184fff9414f7e085a8b8733");
            Assert.AreEqual(serviceResult.UserId, 1);

        }


        [TestMethod]
        public void CreateOrderedItemTest()
        {
            var mockRep = new Mock<IGenericRepository<OrderedItemDto>>();

            OrderedItemDto newOrderedItem = new OrderedItemDto()
            {
                Id = "6183ad6aaf116b389d5d2932",
                ProductId = 2,
                Product = new ProductDto()
                {
                    Id = 2,
                    Name = "testProduct22",
                    Price = 1.2f,
                    Description = "desc",
                    Maker = "none",
                    Count = 3,
                },
                Amount = 2,
                UserId = 1,
            };

            mockRep.Setup(repo => repo.Create(newOrderedItem)).Returns(true).Verifiable();

            this.service = new OrderedItemService(mockRep.Object);

            var result = this.service.CreateEntity(newOrderedItem);

            Assert.IsTrue(result);

        }

        [TestMethod]
        public void UpdateCartTest()
        {
            var mockRep = new Mock<IGenericRepository<CartDto>>();

            CartDto newProduct = this.GetSingleCart();


            object id = "6184fff9414f7e085a8b8733";

            mockRep.Setup(repo => repo.IsExist(id, q => q.Id == id.ToString())).Returns(true).Verifiable();
            mockRep.Setup(repo => repo.Update(newProduct)).Returns(true).Verifiable();

            this.service = new CartService(mockRep.Object);

            var result = this.service.UpdateEntity(id, newProduct);

            Assert.IsTrue(result);

        }  */

        [TestMethod]
        public void RemoveCartByIdTest()
        {
            var mockRep = new Mock<IGenericRepository<CartDto>>();
            var id = 1;

            mockRep.Setup(repo => repo.Remove(id)).Returns(true).Verifiable();

            this.service = new CartService(mockRep.Object);

            var result = this.service.RemoveEntity(id);

            Assert.IsTrue(result);
        }

        private List<CartDto> GetCarts()
        {
            List<CartDto> carts = new List<CartDto>();

            carts.Add(new CartDto
            {
                Id = "6184fff9414f7e085a8b8733",
                UserId = 1,
                OrderedItem = new List<OrderedItemDto>()
                {
                    new OrderedItemDto()
                    {
                        Id = "6183ad6aaf116b389d5d2932",
                        ProductId = 2,
                        Product = new ProductDto()
                        {
                            Id = 2,
                            Name = "testProduct22",
                            Price = 1.2f,
                            Description = "desc",
                            Maker = "none",
                            Count = 3,
                        },
                        Amount = 2,
                        UserId = 1,
                    },
                },
                User = new UserViewDto()
                {
                    Id = 1,
                    Username = "test1",
                    Email = "test1@mail.com",
                    Adress = "adr st, 1",
                    PhoneNumber = "123",
                },
                Status = "Init",
            });

            carts.Add(new CartDto
            {
                Id = "61767ddd955557aa49117c8c",
                UserId = 1,
                OrderedItem = new List<OrderedItemDto>()
                {
                    new OrderedItemDto()
                    {
                        Id = "6183ad6aaf116b389d5d2932",
                        ProductId = 2,
                        Product = new ProductDto()
                        {
                            Id = 2,
                            Name = "testProduct22",
                            Price = 1.2f,
                            Description = "desc",
                            Maker = "none",
                            Count = 3,
                        },
                        Amount = 2,
                        UserId = 1,
                    },
                },
                User = new UserViewDto()
                {
                    Id = 1,
                    Username = "test1",
                    Email = "test1@mail.com",
                    Adress = "adr st, 1",
                    PhoneNumber = "123",
                },
                Status = "Init",
            });

            return carts;
        }

        private CartDto GetSingleCart()
        {
            return new CartDto()
            {
                Id = "6184fff9414f7e085a8b8733",
                UserId = 1,
                OrderedItem = new List<OrderedItemDto>()
                {
                    new OrderedItemDto()
                    {
                        Id = "6183ad6aaf116b389d5d2932",
                        ProductId = 2,
                        Product = new ProductDto()
                        {
                            Id = 2,
                            Name = "testProduct22",
                            Price = 1.2f,
                            Description = "desc",
                            Maker = "none",
                            Count = 3,
                        },
                        Amount = 2,
                        UserId = 1,
                    },
                },
                User = new UserViewDto()
                {
                    Id = 1,
                    Username = "test1",
                    Email = "test1@mail.com",
                    Adress = "adr st, 1",
                    PhoneNumber = "123",
                },
                Status = "Init",
            };
        }
    }
}
