﻿namespace RestShop.Tests.ServiceTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using RestShop.Contracts.DTOs;
    using RestShop.DAL.Interfaces;
    using RestShop.Services.Services;

    [TestClass]
    public class OrderedItemServiceTests
    {
        private OrderedItemService service;

        [TestMethod]
        public void GetProductsTest()
        {
            var mockRep = new Mock<IGenericRepository<OrderedItemDto>>();
            var products = this.GetOrderedItems();
            mockRep.Setup(repo => repo.FindAll(null, null, null)).Returns(products).Verifiable();

            this.service = new OrderedItemService(mockRep.Object);

            List<OrderedItemDto> serviceResult = (List<OrderedItemDto>)this.service.GetAll();

            Assert.AreEqual(serviceResult.Count, 2);
        }

        [TestMethod]
        public void GetOrderedItemByIdTest()
        {
            var mockRep = new Mock<IGenericRepository<OrderedItemDto>>();
            var products = this.GetOrderedItems();

            object id = "6182b9bbc74a60e2a3fac709";

            mockRep.Setup(repo => repo.IsExist(id, q => q.Id == id.ToString())).Returns(true).Verifiable();
            mockRep.Setup(repo => repo.GetById(id)).Returns(products[0]).Verifiable();

            this.service = new OrderedItemService(mockRep.Object);

            OrderedItemDto serviceResult = this.service.GetById("6182b9bbc74a60e2a3fac709");

            Assert.AreEqual(serviceResult.Id, "6182b9bbc74a60e2a3fac709");
            Assert.AreEqual(serviceResult.UserId, 1);
        }

        // Check later. Need mock for http client

        /*
        [TestMethod]
        public void CreateOrderedItemTest()
        {
            var mockRep = new Mock<IGenericRepository<OrderedItemDto>>();

            OrderedItemDto newOrderedItem = new OrderedItemDto()
            {
                Id = "6183ad6aaf116b389d5d2932",
                ProductId = 2,
                Product = new ProductDto()
                {
                    Id = 2,
                    Name = "testProduct22",
                    Price = 1.2f,
                    Description = "desc",
                    Maker = "none",
                    Count = 3,
                },
                Amount = 2,
                UserId = 1,
            };

            mockRep.Setup(repo => repo.Create(newOrderedItem)).Returns(true).Verifiable();

            this.service = new OrderedItemService(mockRep.Object);

            var result = this.service.CreateEntity(newOrderedItem);

            Assert.IsTrue(result);

        }
        */
        [TestMethod]
        public void UpdateOrderedItemTest()
        {
            var mockRep = new Mock<IGenericRepository<OrderedItemDto>>();

            OrderedItemDto newProduct = new OrderedItemDto()
            {
                Id = "6183ad6aaf116b389d5d2932",
                ProductId = 2,
                Product = new ProductDto()
                {
                    Id = 2,
                    Name = "testProduct22",
                    Price = 1.2f,
                    Description = "desc",
                    Maker = "none",
                    Count = 3,
                },
                Amount = 2,
                UserId = 1,
            };

            object id = "6182b9bbc74a60e2a3fac709";

            mockRep.Setup(repo => repo.IsExist(id, q => q.Id == id.ToString())).Returns(true).Verifiable();
            mockRep.Setup(repo => repo.Update(newProduct)).Returns(newProduct).Verifiable();

            this.service = new OrderedItemService(mockRep.Object);

            var result = this.service.UpdateEntity(id, newProduct);

            Assert.AreEqual(result.Id, newProduct.Id);
        }

        [TestMethod]
        public void RemoveOrderedItemByIdTest()
        {
            var mockRep = new Mock<IGenericRepository<OrderedItemDto>>();
            var id = 1;

            mockRep.Setup(repo => repo.Remove(id)).Returns(true).Verifiable();

            this.service = new OrderedItemService(mockRep.Object);

            var result = this.service.RemoveEntity(id);

            Assert.IsTrue(result);
        }

        private List<OrderedItemDto> GetOrderedItems()
        {
            List<OrderedItemDto> products = new List<OrderedItemDto>();

            products.Add(new OrderedItemDto
            {
                Id = "6182b9bbc74a60e2a3fac709",
                ProductId = 2,
                Product = new ProductDto()
                {
                    Id = 2,
                    Name = "testProduct22",
                    Price = 1.2f,
                    Description = "desc",
                    Maker = "none",
                    Count = 3,
                },
                Amount = 2,
                UserId = 1,
            });

            products.Add(new OrderedItemDto
            {
                Id = "6182b8f6d422bab701648061",
                ProductId = 1,
                Product = new ProductDto()
                {
                    Id = 1,
                    Name = "testProduct1",
                    Price = 21f,
                    Description = "desc",
                    Maker = "none",
                    Count = 3,
                },
                Amount = 2,
                UserId = 1,
            });

            return products;
        }
    }
}
