﻿namespace RestShop.Tests.ServiceTests
{
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using RestShop.Contracts.DTOs;
    using RestShop.DAL.Interfaces;
    using RestShop.Services.Services;

    [TestClass]
    public class ProductServiceTests
    {
        private ProductService service;

        [TestMethod]
        public void GetProductsTest()
        {
            var mockRep = new Mock<IGenericRepository<ProductDto>>();
            var products = this.GetProducts();
            mockRep.Setup(repo => repo.FindAll(null, null, null)).Returns(products).Verifiable();

            this.service = new ProductService(mockRep.Object);

            List<ProductDto> serviceResult = (List<ProductDto>)this.service.GetAll();

            Assert.AreEqual(serviceResult.Count, 2);
        }

        [TestMethod]
        public void GetProductByIdTest()
        {
            var mockRep = new Mock<IGenericRepository<ProductDto>>(MockBehavior.Default);
            var products = this.GetProducts();
            var id = 1;
            mockRep.Setup(repo => repo.IsExist(id, q => q.Id == (int)id)).Returns(true).Verifiable();
            mockRep.Setup(repo => repo.GetById(id)).Returns(products[0]).Verifiable();

            this.service = new ProductService(mockRep.Object);

            ProductDto serviceResult = this.service.GetById(1);

            Assert.AreEqual(serviceResult.Id, 1);
            Assert.AreEqual(serviceResult.Name, "test1");
        }

        [TestMethod]
        public void CreateProductTest()
        {
            var mockRep = new Mock<IGenericRepository<ProductDto>>();
            ProductDto newProduct = new ProductDto()
            {
                Id = 3,
                Name = "test1",
                Description = "some desc",
                Maker = "none",
                Count = 3,
                Price = 12.2f,
            };

            mockRep.Setup(repo => repo.Create(newProduct)).Returns(newProduct).Verifiable();

            this.service = new ProductService(mockRep.Object);

            var result = this.service.CreateEntity(newProduct);

            Assert.AreEqual(result.Name, newProduct.Name);
        }

        [TestMethod]
        public void UpdateProductTest()
        {
            var mockRep = new Mock<IGenericRepository<ProductDto>>();

            ProductDto newProduct = new ProductDto()
            {
                Id = 3,
                Name = "test1",
                Description = "some desc",
                Maker = "none",
                Count = 3,
                Price = 12.2f,
            };

            var id = 1;

            mockRep.Setup(repo => repo.IsExist(id, q => q.Id == (int)id)).Returns(true).Verifiable();
            mockRep.Setup(repo => repo.Update(newProduct)).Returns(newProduct).Verifiable();

            this.service = new ProductService(mockRep.Object);

            var result = this.service.UpdateEntity(id, newProduct);

            Assert.AreEqual(result.Name, newProduct.Name);
        }

        [TestMethod]
        public void RemoveProductByIdTest()
        {
            var mockRep = new Mock<IGenericRepository<ProductDto>>();
            var id = 1;

            mockRep.Setup(repo => repo.Remove(id)).Returns(true).Verifiable();

            this.service = new ProductService(mockRep.Object);

            var result = this.service.RemoveEntity(id);

            Assert.IsTrue(result);
        }

        private List<ProductDto> GetProducts()
        {
            List<ProductDto> products = new List<ProductDto>();

            products.Add(new ProductDto
            {
                Id = 1,
                Name = "test1",
                Description = "some desc",
                Maker = "none",
                Count = 3,
                Price = 12.2f,
            });

            products.Add(new ProductDto
            {
                Id = 2,
                Name = "test2",
                Description = "some desc",
                Maker = "none",
                Count = 1,
                Price = 10.2f,
            });

            return products;
        }
    }
}
