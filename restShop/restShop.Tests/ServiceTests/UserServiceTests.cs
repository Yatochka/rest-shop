﻿namespace RestShop.Tests.ServiceTests
{
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using RestShop.Contracts.DTOs;
    using RestShop.DAL.Interfaces;
    using RestShop.Services.Services;

    [TestClass]
    public class UserServiceTests
    {
        private UserService service;

        [TestMethod]
        public void GetUsersTest()
        {
            var mockRep = new Mock<IGenericRepository<UserDto>>();
            var users = this.GetUsers();
            mockRep.Setup(repo => repo.FindAll(null, null, null)).Returns(users).Verifiable();

            this.service = new UserService(mockRep.Object);

            List<UserDto> serviceResult = (List<UserDto>)this.service.GetAll();

            Assert.AreEqual(serviceResult.Count, 2);
        }

        [TestMethod]
        public void GetUsersByIdTest()
        {
            var mockRep = new Mock<IGenericRepository<UserDto>>(MockBehavior.Default);
            var users = this.GetUsers();
            var id = 1;
            mockRep.Setup(repo => repo.IsExist(id, q => q.Id == (int)id)).Returns(true).Verifiable();
            mockRep.Setup(repo => repo.GetById(id)).Returns(users[0]).Verifiable();

            this.service = new UserService(mockRep.Object);

            UserDto serviceResult = this.service.GetById(1);

            Assert.AreEqual(serviceResult.Id, 1);
            Assert.AreEqual(serviceResult.Username, "test1");
        }

        [TestMethod]
        public void CreateUserTest()
        {
            var mockRep = new Mock<IGenericRepository<UserDto>>();
            UserDto newUsr = new UserDto()
            {
                Id = 3,
                Username = "test3",
                Email = "test3@mail.com",
                Password = "123123",
                PhoneNumber = "123",
                Adress = "testA",
            };

            mockRep.Setup(repo => repo.Create(newUsr)).Returns(newUsr).Verifiable();

            this.service = new UserService(mockRep.Object);

            var result = this.service.CreateEntity(newUsr);

            Assert.AreEqual(result.Username, newUsr.Username);
        }

        [TestMethod]
        public void UpdateUserTest()
        {
            var mockRep = new Mock<IGenericRepository<UserDto>>();
            var users = this.GetUsers();

            UserDto newUsr = new UserDto()
            {
                Id = 1,
                Username = "test3",
                Email = "test1@mail.com",
                Adress = "adr st, 1",
                Password = "123",
                PhoneNumber = "123",
            };

            var id = 1;

            mockRep.Setup(repo => repo.IsExist(id, q => q.Id == (int)id)).Returns(true).Verifiable();
            mockRep.Setup(repo => repo.Update(newUsr)).Returns(newUsr).Verifiable();

            this.service = new UserService(mockRep.Object);

            var result = this.service.UpdateEntity(id, newUsr);

            Assert.AreEqual(result.Username, newUsr.Username);
        }

        [TestMethod]
        public void RemoveUserByIdTest()
        {
            var mockRep = new Mock<IGenericRepository<UserDto>>();
            var id = 1;

            mockRep.Setup(repo => repo.Remove(id)).Returns(true).Verifiable();

            this.service = new UserService(mockRep.Object);

            var result = this.service.RemoveEntity(id);

            Assert.IsTrue(result);
        }

        private List<UserDto> GetUsers()
        {
            List<UserDto> users = new List<UserDto>();

            users.Add(new UserDto
            {
                Id = 1,
                Username = "test1",
                Email = "test1@mail.com",
                Adress = "adr st, 1",
                Password = "123",
                PhoneNumber = "123",
            });

            users.Add(new UserDto
            {
                Id = 2,
                Username = "test2",
                Email = "test2@mail.com",
                Adress = "adr st, 2",
                Password = "123",
                PhoneNumber = "123",
            });
            return users;
        }
    }
}
