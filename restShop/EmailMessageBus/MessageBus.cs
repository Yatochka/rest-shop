﻿using Microsoft.Extensions.Hosting;
using RestShop.Contracts.Delegates;
using RestShop.ElasticSearch;
using RestShop.Services.Services;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace RestShop.EmailMessageBus
{
    public class MessageBus : IHostedService, IDisposable // BackgroundService
    {
        private Timer timer = null;
        private readonly RabbitMQClient rabbitClient;
        private readonly MessageSender sender;
        private ElasticSearchClient elasticClient;

        public MessageBus()
        {
            this.elasticClient = new ElasticSearchClient();
            // for sending emails
            // this.sender = EmailSender.Email;
            this.sender = this.elasticClient.SendToElastic;
            this.rabbitClient = new RabbitMQClient();
        }

        public void SendToEmail(object? state)
        {
            this.rabbitClient.ReceiveRabbitMessage(this.sender);
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {

            this.timer = new Timer(
                this.SendToEmail,
                null,
                TimeSpan.Zero,
                TimeSpan.FromSeconds(5));
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
           return Task.CompletedTask;
        }

        public void Dispose()
        {
            this.rabbitClient.Dispose();
        }
    }
}
