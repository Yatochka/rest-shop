﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestShop.EmailMessageBus
{
    public class EmailSenderConfigs
    {
        public string FromEmail { get; set; }

        public string FromEmailPassword { get; set; }

        public string ToEmail { get; set; }

        public string Host { get; set; }

        public int Port { get; set; }
    }
}
