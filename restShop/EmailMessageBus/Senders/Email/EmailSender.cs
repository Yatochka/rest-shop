﻿namespace RestShop.EmailMessageBus
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using Newtonsoft.Json;

    public class EmailSender
    {
        private  readonly EmailSenderConfigs configs;

        public EmailSender()
        {
            using (StreamReader r = new StreamReader(Directory.GetCurrentDirectory()+"\\emailsenderconfigs.json"))
            {
                string json = r.ReadToEnd();
                this.configs = JsonConvert.DeserializeObject<EmailSenderConfigs>(json);
            }
        }

        public void Email(string htmlString)
        {
            try
            {
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress(this.configs.FromEmail); 
                message.To.Add(new MailAddress(this.configs.ToEmail)); 
                message.Subject = "Order Created";
                message.IsBodyHtml = true; //to make message body as html  
                message.Body = htmlString;
                smtp.Port = this.configs.Port; 
                smtp.Host = this.configs.Host; //for gmail host  
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(this.configs.FromEmail, this.configs.FromEmailPassword);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
