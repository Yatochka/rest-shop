﻿namespace RestShop.DAL.SQL.Interfaces
{
    using RestShop.DAL.Interfaces;

    public interface ISQLRepository<TDto, TEntity> : IGenericRepository<TDto>
        where TEntity : class
        where TDto : class
    {
    }
}
