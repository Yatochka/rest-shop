﻿namespace RestShop.DAL.SQL.DbConnection
{
    using RestShop.DAL.Context;

    public static class SqlDbConnection
    {
        private static readonly ApplicationDbContext ApplicationDbContext;

        public static ApplicationDbContext GetDataBase()
        {
            if (ApplicationDbContext != null)
            {
                return ApplicationDbContext;
            }

          // applicationDbContext = new ApplicationDbContext(DbContextOptions<ApplicationDbContext>(options => options.SqlServer("")));
            return ApplicationDbContext;
        }
    }
}
