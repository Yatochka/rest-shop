﻿namespace RestShop.DAL.SQL.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using Microsoft.EntityFrameworkCore;
    using RestShop.Contracts.ShopMapper;
    using RestShop.DAL.Context;
    using RestShop.DAL.SQL.Interfaces;

    public class SqlGenericRepository<TDto, TEntity> : ISQLRepository<TDto, TEntity>
        where TEntity : class
        where TDto : class
    {
        private readonly ApplicationDbContext context;
        private readonly DbSet<TEntity> db;
        private readonly ShopMapper<TEntity, TDto> mapper;

        public SqlGenericRepository(ApplicationDbContext context)
        {
            this.context = context;
            this.db = this.context.Set<TEntity>();
            this.mapper = new ShopMapper<TEntity, TDto>();
        }

        public TDto Create(TDto entity)
        {
            var toCreate = this.mapper.MapDtoToEntity(entity);
            this.db.Add(toCreate);
            this.context.SaveChanges();
            return entity;
        }

        public IEnumerable<TDto> FindAll(
            Expression<Func<TDto, bool>> filter = null,
            Func<IQueryable<TDto>, IOrderedQueryable<TDto>> orderBy = null,
            List<string> includes = null)
        {
            IQueryable<TEntity> query = this.db;
            var entFilter = this.mapper.ConvertFilter(filter);

            if (filter != null)
            {
                query = query.Where(entFilter);
            }

            if (includes != null)
            {
                foreach (var table in includes)
                {
                    query = query.Include(table);
                }
            }

            /* fix orderby or delete it
            if (orderBy != null)
            {

                return this.mapper.MapToDtoList(orderBy(query).ToList());
            }
            else
            {
                return this.mapper.MapToDtoList(query.ToList());
            }*/
            return this.mapper.MapToDtoList(query.ToList());
         }

        public TDto GetById(object id)
         {
            var foundObject = this.mapper.MapEntityToDto(this.db.Find(id));
            return foundObject;
         }

        public bool IsExist(object id, Expression<Func<TDto, bool>> filter = null)
         {
            var query = this.db.Find(id);
            this.context.Entry(query).State = EntityState.Detached;
            return query != null;
         }

        public bool Remove(TDto entity)
         {
            var toRemove = this.mapper.MapDtoToEntity(entity);
            this.db.Remove(toRemove);
            return this.context.SaveChanges() == 1;
         }

        public bool Remove(object id)
         {
            var toDelete = this.GetById(id);
            return this.Remove(toDelete);
         }

        public TDto Update(TDto entity)
         {
            var toUpdate = this.mapper.MapDtoToEntity(entity);
            this.db.Update(toUpdate);
            this.context.SaveChanges();
            return entity;
         }
    }
}
